package com.binarystudio.academy.springsecurity.security.auth;

import com.binarystudio.academy.springsecurity.domain.user.model.User;
import com.binarystudio.academy.springsecurity.security.auth.model.*;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("auth")
public class AuthController {
	private final AuthService authService;

	public AuthController(AuthService authService) {
		this.authService = authService;
	}

	@PostMapping("safe/login")
	public AuthResponse login(@RequestBody AuthorizationRequest authorizationRequest) {
		return authService.performLogin(authorizationRequest);
	}

	@PostMapping("safe/register")
	public AuthResponse register(@RequestBody RegistrationRequest registrationRequest) {
		// 1. implement registration
		return authService.register(registrationRequest);
	}

	@PostMapping("safe/refresh")
	public AuthResponse refreshTokenPair(@RequestBody RefreshTokenRequest refreshTokenRequest) {
		return authService.refreshTokenPair(refreshTokenRequest);
	}

	@PutMapping("safe/forgotten_password")
	public void forgotPasswordRequest(@RequestParam String email) {
		System.out.println("E-mail sent to " + email);
		System.out.println(authService.refreshTokenPair(email).getAccessToken());
	}

	@PatchMapping("safe/forgotten_password")
	public AuthResponse forgottenPasswordReplacement(@RequestBody ForgottenPasswordReplacementRequest forgottenPasswordReplacementRequest) {
		return authService.forgottenPasswordReplacement(forgottenPasswordReplacementRequest);
	}

	@PatchMapping("change_password")
	public AuthResponse changePassword(@RequestBody PasswordChangeRequest passwordChangeRequest, @AuthenticationPrincipal User user) {
		return authService.changePassword(passwordChangeRequest, user);
	}

	@GetMapping("me")
	public User whoAmI(@AuthenticationPrincipal User user) {
		return user;
	}
}
