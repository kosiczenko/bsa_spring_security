package com.binarystudio.academy.springsecurity.security.auth;

import com.binarystudio.academy.springsecurity.domain.user.UserService;
import com.binarystudio.academy.springsecurity.domain.user.model.User;
import com.binarystudio.academy.springsecurity.security.auth.model.AuthResponse;
import com.binarystudio.academy.springsecurity.security.auth.model.AuthorizationRequest;
import com.binarystudio.academy.springsecurity.security.auth.model.ForgottenPasswordReplacementRequest;
import com.binarystudio.academy.springsecurity.security.auth.model.PasswordChangeRequest;
import com.binarystudio.academy.springsecurity.security.auth.model.RefreshTokenRequest;
import com.binarystudio.academy.springsecurity.security.auth.model.RegistrationRequest;
import com.binarystudio.academy.springsecurity.security.jwt.JwtProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.server.ResponseStatusException;

@Service
public class AuthService {
	private final UserService userService;
	private final JwtProvider jwtProvider;
	private final PasswordEncoder passwordEncoder;

	public AuthService(UserService userService, JwtProvider jwtProvider, PasswordEncoder passwordEncoder) {
		this.userService = userService;
		this.jwtProvider = jwtProvider;
		this.passwordEncoder = passwordEncoder;
	}

	public AuthResponse performLogin(AuthorizationRequest authorizationRequest) {
		var userDetails = userService.loadUserByUsername(authorizationRequest.getUsername());
		if (passwordsDontMatch(authorizationRequest.getPassword(), userDetails.getPassword())) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid password");
		}
		return AuthResponse.of(jwtProvider.generateAccessToken(userDetails), jwtProvider.generateRefreshToken(userDetails));
	}

	private boolean passwordsDontMatch(String rawPw, String encodedPw) {
		return !passwordEncoder.matches(rawPw, encodedPw);
	}

	public AuthResponse register(RegistrationRequest registrationRequest) {
		User user = userService.createUserByRegistrationRequest(registrationRequest);
		user.setPassword(passwordEncoder.encode(registrationRequest.getPassword()));
		return AuthResponse.of(jwtProvider.generateAccessToken(user), jwtProvider.generateRefreshToken(user));
	}

	public AuthResponse changePassword(PasswordChangeRequest passwordChangeRequest, User user) {
		user.setPassword(passwordEncoder.encode(passwordChangeRequest.getNewPassword()));
		return AuthResponse.of(jwtProvider.generateAccessToken(user), jwtProvider.generateRefreshToken(user));
	}

	public AuthResponse refreshTokenPair(RefreshTokenRequest refreshTokenRequest) {
		var login = jwtProvider.getLoginFromToken(refreshTokenRequest.getRefreshToken());
		if(login == null ) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Bad refresh token");
		}
		User user = userService.loadUserByUsername(login);
		return AuthResponse.of(jwtProvider.generateAccessToken(user), jwtProvider.generateRefreshToken(user));
	}

	public AuthResponse refreshTokenPair(String email) {
		if(email == null ) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No such email");
		}
		User user = userService.findUserByEmail(email).get();
		return AuthResponse.of(jwtProvider.generateAccessToken(user), jwtProvider.generateRefreshToken(user));
	}

	public AuthResponse forgottenPasswordReplacement(ForgottenPasswordReplacementRequest forgottenPasswordReplacementRequest) {
		var login = jwtProvider.getLoginFromToken(forgottenPasswordReplacementRequest.getToken());
		if(login == null ) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Bad password recovery token");
		}
		User user = userService.loadUserByUsername(login);
		user.setPassword(passwordEncoder.encode(forgottenPasswordReplacementRequest.getNewPassword()));
		return AuthResponse.of(jwtProvider.generateAccessToken(user), jwtProvider.generateRefreshToken(user));
	}
}
