package com.binarystudio.academy.springsecurity.domain.hotel;

import com.binarystudio.academy.springsecurity.domain.hotel.model.Hotel;
import com.binarystudio.academy.springsecurity.domain.user.model.User;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

@Service
public class HotelService {
    private final HotelRepository hotelRepository;

    public HotelService(HotelRepository hotelRepository) {
        this.hotelRepository = hotelRepository;
    }

    public void delete(UUID hotelId, User user) {
        // 4. only the owner of the hotel or admin may delete the hotel
        Hotel hotel = hotelRepository.getById(hotelId).orElseThrow(NoSuchElementException::new);
        if (user.isAdmin() || isHotelOwner(user, hotel)) {
            boolean wasDeleted = hotelRepository.delete(hotelId);
            if (!wasDeleted) {
                throw new NoSuchElementException();
            }
        } else {
            throw new AccessDeniedException("Access denied");
        }
    }

    private boolean isHotelOwner(User user, Hotel hotel) {
        return hotel.getOwnerId().equals(user.getId());
    }

    public List<Hotel> getAll() {
        return hotelRepository.getHotels();
    }


    public Hotel update(Hotel hotel, User user) {
        // 4. only the owner of the hotel or admin may update the hotel
        if (user.isAdmin() || isHotelOwner(user, hotel)) {
            getById(hotel.getId());
            return hotelRepository.save(hotel);
        } else {
            throw new AccessDeniedException("Access denied");
        }
    }

    public Hotel create(Hotel hotel) {
        return hotelRepository.save(hotel);
    }

    public Hotel getById(UUID hotelId) {
        return hotelRepository.getById(hotelId).orElseThrow(NoSuchElementException::new);
    }
}
